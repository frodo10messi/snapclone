//
//  SelectPictureViewController.swift
//  snapclone
//
//  Created by SGI-Mac7 on 30/10/2018.
//  Copyright © 2018 Slash Global. All rights reserved.
//

import UIKit
import FirebaseStorage
import SVProgressHUD

class SelectPictureViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate
 {

    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    var imagePicker : UIImagePickerController?
    var imageAdded = false
    var imageName = "\(NSUUID().uuidString).jpg"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker = UIImagePickerController()
        imagePicker?.delegate = self
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?){
        view.endEditing(true)
        super.touchesBegan(touches, with: event)
    }
    
    @IBAction func selectPhotoTapped(_ sender: Any) {
        if imagePicker != nil {
            imagePicker!.sourceType = .photoLibrary
            present(imagePicker!, animated: true, completion: nil)
        }
    }
    @IBAction func cameraTapped(_ sender: Any) {
        if imagePicker != nil {
            imagePicker!.sourceType = .camera
            present(imagePicker!, animated: true, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = image
            imageAdded = true
        }
        
        dismiss(animated: true, completion: nil)
    }
    func presentAlert(alert:String) {
        let alertVC = UIAlertController(title: "Error", message: alert, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            alertVC.dismiss(animated: true, completion: nil)
        }
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
    }
    
    
    @IBAction func nextTapped(_ sender: Any) {
        
        // DELETE THIS FOR PRODUCTION
        //messageTextField.text = "test"
        //imageAdded = true
//        var url1:URL!
        SVProgressHUD.show()
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        
        if let message = messageTextField.text {
            if imageAdded && message != "" {
                // Upload the image
                
                let imagesFolder = Storage.storage().reference().child("images")
                
                if let image = imageView.image {
                    if let imageData = UIImageJPEGRepresentation(image, 0.1) {
                        imagesFolder.child(imageName).putData(imageData, metadata: metaData, completion: { (metadata, error) in
                            if let error = error {
                                SVProgressHUD.dismiss()
                                self.presentAlert(alert: error.localizedDescription)
                            }
                            else {
                            
                                imagesFolder.child(self.imageName).downloadURL { (url, error) in
                                    guard let downloadURL = url else {
                                        SVProgressHUD.dismiss()
                                        self.presentAlert(alert: (error?.localizedDescription)!);                                        return
                                    }
                                    
                                    
                                     let url1 = downloadURL.absoluteString
                                        
                                    
                                    SVProgressHUD.dismiss()
                                    self.performSegue(withIdentifier: "selectReciverSegue", sender: url1)                            }
                                    
                                    
                                    
                                    
                                                                      }
                              
                            
                                    
                                   
                                
                            }
                        )
                    }
                    
                }
            }
        
        else {
                // We are missing something
                SVProgressHUD.dismiss()
                presentAlert(alert: "You must provide an image and a message for your snap.")
            }
        
    }
        
}
  
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let downloadURL = sender as? String {
            print(downloadURL)
            if let selectVC = segue.destination as? SelectRecipientViewController {
                selectVC.downloadURL = downloadURL
                
                selectVC.snapDescription = messageTextField.text!
                print(messageTextField.text!)
                selectVC.imageName = imageName
            }
        }
    }
    
    
    
    
}
