//
//  SelectRecipientViewController.swift
//  snapclone
//
//  Created by SGI-Mac7 on 30/10/2018.
//  Copyright © 2018 Slash Global. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class User {
    var email = ""
    var uid = ""
}
class SelectRecipientViewController:UITableViewController {

    var snapDescription = ""
    var downloadURL = ""
    var imageName = ""
    var users = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("hello \(snapDescription)")
        print(" hello\(downloadURL)")
        Database.database().reference().child("users").observe(.childAdded) { (snapshot) in
            let user = User()
            if let userDictionary = snapshot.value as? NSDictionary {
                if let email = userDictionary["email"] as? String {
                    print(email)
                    user.email = email
                    user.uid = snapshot.key
                    self.users.append(user)
                    self.tableView.reloadData()
                    print(self.users[0].email)
                }
            }
        }
    }
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return users.count
        }
        
        
       override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            
        
            cell.textLabel?.text = users[indexPath.row].email
            
            return cell
        }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let user = users[indexPath.row]
        
        if let fromEmail = Auth.auth().currentUser?.email {
            
            let snap = ["from":fromEmail,"description":snapDescription,"imageURL":downloadURL,"imageName":imageName]
            
            Database.database().reference().child("users").child(user.uid).child("snaps").childByAutoId().setValue(snap)
            
            navigationController?.popToRootViewController(animated: true)
        }
    }


}
