//
//  LoginViewController.swift
//  Snapchat
//
//  Created by zappycode on 6/30/17.
//  Copyright © 2017 Nick Walter. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import SVProgressHUD
import SwiftKeychainWrapper

class LoginViewController: UIViewController {
    
    @IBOutlet weak var topButton: UIButton!
    @IBOutlet weak var bottomButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    var signupMode = false
    var currentUserID = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        if  let uid = KeychainWrapper.standard.string(forKey: "accesstoken"){
            print("NOTE: ID found in keychain \(uid)")
            self.performSegue(withIdentifier: "moveToSnaps", sender: nil)
            
        }
    }

    @IBAction func topTapped(_ sender: Any) {
        SVProgressHUD.show()
        if let email = emailTextField.text {
            if let password = passwordTextField.text {
                if signupMode {
                    // Sign Up
                    Auth.auth().createUser(withEmail: email, password: password, completion: { (user, error) in
                        if let error = error {
                            self.presentAlert(alert: error.localizedDescription)
                        } else {
                            if user != nil {
                                
                                if let uid = Auth.auth().currentUser?.uid {
                                    if let email = Auth.auth().currentUser?.email{
                Database.database().reference().child("users").child(uid).child("email").setValue(email)
                                        
                                        let keychainresult = KeychainWrapper.standard.set(uid, forKey: "accesstoken")
                                        print("data saved to keychain \(keychainresult)")
                                        
                                        SVProgressHUD.dismiss()

                                        self.performSegue(withIdentifier: "moveToSnaps", sender: nil)                                    }
                                }
                            }
                        }
                    })
                } else {
                    // Log In
                    Auth.auth().signIn(withEmail: email, password: password, completion: { (user, error) in
                        if let error = error {
                            SVProgressHUD.dismiss()
                            self.presentAlert(alert: error.localizedDescription)
                        } else {
                            SVProgressHUD.dismiss()
                            if let uid = Auth.auth().currentUser?.uid{
                            let keychainresult = KeychainWrapper.standard.set(uid, forKey: "accesstoken")
                            print("data saved to keychain \(keychainresult)")
                            
                            self.performSegue(withIdentifier: "moveToSnaps", sender: nil)
                            }
                        }
                    })
                }
            }
        }
    }
    
    func presentAlert(alert:String) {
        let alertVC = UIAlertController(title: "Error", message: alert, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            alertVC.dismiss(animated: true, completion: nil)
        }
        alertVC.addAction(okAction)
        present(alertVC, animated: true, completion: nil)
    }
    
    @IBAction func bottomTapped(_ sender: Any) {
        if signupMode {
            // Switch to Log In
            signupMode = false
            topButton.setTitle("Log In", for: .normal)
            bottomButton.setTitle("Switch to Sign Up", for: .normal)
        } else {
            // Switch to Sign Up
            signupMode = true
            topButton.setTitle("Sign Up", for: .normal)
            bottomButton.setTitle("Switch to Log In", for: .normal)
        }
    }
    
}

