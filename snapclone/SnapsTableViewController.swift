//
//  SnapsTableViewController.swift
//  snapclone
//
//  Created by SGI-Mac7 on 30/10/2018.
//  Copyright © 2018 Slash Global. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import SwiftKeychainWrapper

class SnapsTableViewController: UITableViewController {

    var snaps = [DataSnapshot]()

    override func viewDidLoad() {
        super.viewDidLoad()

        if let currentUserUid = Auth.auth().currentUser?.uid {
            Database.database().reference().child("users").child(currentUserUid).child("snaps").observe(.childAdded, with: { (snapshot) in
                self.snaps.append(snapshot)
                self.tableView.reloadData()
                print(snapshot)
            })
            
            
            
            //logic for removing snap from array
            Database.database().reference().child("users").child(currentUserUid).child("snaps").observe(.childRemoved, with: { (snapshot) in
                
                var index = 0
                for snap in self.snaps {
                    if snapshot.key == snap.key {
                        self.snaps.remove(at: index)
                    }
                    index += 1
                }
                self.tableView.reloadData()
            })
     
            
            
            
        }
        
    }
    

    @IBAction func logOutTapped(_ sender: Any) {
        try? Auth.auth().signOut()
        let removeSuccessful: Bool = KeychainWrapper.standard.removeObject(forKey: "accesstoken")
        print(removeSuccessful)
        dismiss(animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if snaps.count == 0 {
            return 1
        } else {
            return snaps.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        if snaps.count == 0 {
            cell.textLabel?.text = "You have no snaps 😔"
        } else {
            let snap = snaps[indexPath.row]
            
            if let snapDictionary = snap.value as? NSDictionary {
                if let fromEmail = snapDictionary["from"] as? String {
                    cell.textLabel?.text = fromEmail
                }
            }
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let snap = snaps[indexPath.row]
        
        performSegue(withIdentifier: "viewSnapSegue", sender: snap)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "viewSnapSegue" {
            if let viewVC = segue.destination as? ViewSnapViewController {
                
                if let snap = sender as? DataSnapshot {
                    
                    viewVC.snap = snap
                }
            }
        }
    }
    
}
